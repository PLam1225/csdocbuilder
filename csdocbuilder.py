#!/usr/bin/python
import os, sys, time
import json
import hashlib
import datetime
import errno
import csv

# need pip install
import requests
import boto3
from lxml import etree

# local import
import outwardrc
import cloudsearch

class CSDocBuilder:

    def __init__(self, xml=None, csv_file=None, sku_list=None):
        self.csv_file = csv_file
        self.sku_list = sku_list
        self.xml_file = xml
        self.xml_list = [xml]
        

        self.products = {}
        self.doc_list = []
    
    def run_all_we_sku_xml(self):
        
        self.xml_list = []

        for root, dirs, filelist in os.walk('/mnt/g/WSI-DF'):
            for filename in filelist:
                if filename == 'we_sku.xml':
                    file_path = os.path.join(root, filename)
                    m_time = int(os.path.getmtime(file_path))
                    if m_time < 1523948400:
                        self.xml_list.append(file_path)
        
        
        self.products = self.xml_to_dict(self.xml_list, self.sku_list)
        self.process_ow_data(self.csv_file)
        self.doc_list = self.prep_doc_list(doc_type='add')
        #print json.dumps(self.doc_list, indent=4)
        
        self.create_batch()

    def run(self):
        self.products = self.xml_to_dict(self.xml_list, self.sku_list)
        self.process_ow_data(self.csv_file)
        self.doc_list = self.prep_doc_list(doc_type='add')
        self.create_batch()

    def delete_doc(self):

        self.doc_list = self.prep_doc_list(doc_type='delete')
        self.create_batch(batch_name='5MB_batch_delete_')
    
    def consolidate_fieldvalue(self):
        
        consolidated_dict = {}
        for sku, product_list in self.products.iteritems():
            fieldvalue_dict = {}
            all_fieldname = list({k for d in product_list for k in d.keys()})

            for fieldname in all_fieldname:
                if fieldname == 'altviewimages':
                    item_list = list({v for d in product_list for v in d.get(fieldname, [])})
                else:
                    item_list = list({d.get(fieldname) for d in product_list if d.get(fieldname) is not None})

                
                
                fieldvalue_dict[fieldname] = item_list

            consolidated_dict[sku] = fieldvalue_dict
        
        return consolidated_dict
                

    def check_valid_file(self, path):
        if not os.path.isfile(path):
            outwardrc.post("Error: cannot find this file: ```{}```".format(path))
            sys.exit(1)

    def create_batch(self, batch_name='batch_upload_'):
        utimestamp = int(time.time())
        batch_name = batch_name + '_' + str(utimestamp)
        doc_count = len(self.doc_list)
        print "doc_count:{}".format(doc_count)
        if doc_count > 0:

            batch_root_path = "/".join([sys.path[0], "batch"])
            batch_dir_name = os.path.splitext(os.path.basename(self.xml_file))[0]
            batch_file_no_id_ext = "/".join([batch_root_path, batch_dir_name, batch_name])

            batch_size_limit = 5000000
            batch_file_count = 0
            batch_file_ext = ".json"
            start_index = 0
            last_index = 0

            if self.create_batch_dir(batch_root_path, batch_dir_name):

                while (last_index < doc_count):
                    
                    print "{}: START batch # creation : start index={}, last_index={}".format(datetime.datetime.now(), batch_file_count, start_index, last_index)
                    start_index = last_index
                    last_index = self.get_last_doc_index(self.doc_list, start_index, doc_count, batch_size_limit)

                    batch_file_count += 1
                    batch_file = batch_file_no_id_ext + str(batch_file_count) + batch_file_ext
                    with open(batch_file, 'w') as f:
                        f.write(json.dumps(self.doc_list[start_index:last_index]))

                        print "{}: END batch # {} created: start index={}, last_index={}".format(datetime.datetime.now(), batch_file_count, start_index, last_index)
            
    def xml_to_dict(self, xml_list, sku_list=None):
    

        existing_SKUs = []
        products = {}
        for xml_file in sorted(xml_list):
            print xml_file
            self.check_valid_file(xml_file)
            for event, element in etree.iterparse(xml_file, tag="Product"):
                
                sku = element.find("Sku").text
                if sku in sku_list:

                    field_dict = {}
                    for child in element:
                        if child.text is not None: # Get only non-empty field values
                            sub_child1 = list(child)
                            if len(sub_child1):
                                sub_child1_texts = []
                                for child1 in sub_child1:
                                    if child1.text is not None:
                                        sub_child1_texts.append(child1.text)
                                field_dict[child.tag.lower()] = sub_child1_texts
                            else:
                                field_dict[child.tag.lower()] = child.text  # cloudsearch does not allow Uppercase char in field name


                    if sku in existing_SKUs:
                        for fieldname, fieldvalue in field_dict.iteritems():
                            current_fieldvalue = products[sku][0].get(fieldname)
                            if (current_fieldvalue is None) or (current_fieldvalue != fieldvalue):
                                products[sku][0][fieldname] = fieldvalue
                    else:
                        existing_SKUs.append(sku)
                        products[sku] = [field_dict]

                element.clear()

        return products

    def process_ow_data(self, csv_file):
        with open(csv_file, 'r') as csv_ref:
            reader = csv.DictReader(csv_ref)

            fields = list(reader.fieldnames)
            fields.remove('sku')
            valid_skus = self.products.keys()

            for row in reader:
                if row['sku'] in valid_skus:
                    dict_count = len(self.products[row['sku']])
                    finish_items = []

                    for field in fields:
                        if 'finish' in field:
                            finish_items.append(row[field])
                        i = 0
                        while i < dict_count:
                            self.products[row['sku']][i][field] = row[field]
                            i += 1
                    self.products[row['sku']][0]['ow_finish'] = finish_items
        
        # get category fields such that "string1 > string2 > string3" correlates to "we_categoryl1 > we_categoryl3 > we_categoryl5"
        for sku in self.products.keys():
            category_value = self.products[sku][0].get('category')

            if category_value is not None:
                category_list = category_value.split('>')
                category_count = len(category_list)

                self.products[sku][0]['we_categoryl1'] = category_list[0].strip().lower()
                if category_count > 1:
                    self.products[sku][0]['we_categoryl3'] = category_list[1].strip().lower()
                    
                    if category_count > 2:
                        self.products[sku][0]['we_categoryl5'] = category_list[2].strip().lower()
            
    def prep_doc_list(self, doc_type='add'):
        non_collision_list = []
        doc_list = []
        fields_to_check = ['ow_oman']

        if doc_type == 'add':
                
            undefined_fieldnames = []
            active_fields = cloudsearch.get_index_fields('outward-cs-test1', state='Active')

            for sku, product_list in self.products.iteritems():
                #if len(product_list) > 1:
                    #collision = self.get_fieldvalue_collision(product_list, fields_to_check)
                    #if collision is not None:
                    #    outwardrc.post("WARNING: field-value collision found from duplicate SKUs id `{}`: ```{}```".format(sku, collision))
                    #    continue
            
                md5 = hashlib.md5(json.dumps(self.products[sku][0], sort_keys=True)).hexdigest()
                #md5_check = self.compare_md5(sku, self.products[sku][0], md5)

                #if md5_check != 'same':
                
                for fieldname in product_list[0].keys():
                    if fieldname not in active_fields:
                        undefined_fieldnames.append(fieldname)
                        del product_list[0][fieldname]

                doc_list.append(self.get_doc_format(doc_type, self.products[sku][0], md5))
            if len(undefined_fieldnames) > 0:
                outwardrc.post("Warning: undefined fields detected. These fields are removed from documents before uploading to cloudsearch ```{}```".format(undefined_fieldnames))

        elif doc_type == 'delete':
            for sku in self.sku_list:
                doc_list.append({"type": "delete", "id": sku})
        else:
            print "invalid doc_type '{}'. Exiting...".format(doc_type)
            sys.exit(1)
        
        print "Document count: {}\n".format(len(doc_list))
        return doc_list
            
    def get_fieldvalue_collision(self, list_of_dict, field_checklist=['ow_oman']):

        diff_fieldvalue_dict = {}
        for fieldname in field_checklist:

            fieldvalues = list({d[fieldname] for d in list_of_dict})

            if len(fieldvalues) > 1:
                diff_fieldvalue_dict[fieldname] = fieldvalues
        
        if len(diff_fieldvalue_dict.keys()) > 0:
            return diff_fieldvalue_dict
        else:
            return None
 
    
    def get_doc_format(self, action_type, fields, md5_value='None'):

        fields['cs_md5'] = md5_value

        doc_dict =  {
                        'type': action_type,
                        'id':   fields['sku'],
                        'fields':   fields
                    }


        return doc_dict

    def compare_md5(self, sku, field_dict, this_md5):


        response = client.search    (
                                        query=sku,
                                        queryOptions= '{"fields":["sku"]}',
                                        returnFields='cs_md5'
                                    )

        if response['hits']['found'] > 0:
            if response['hits']['hit'][0]['fields']['cs_md5'][0] == this_md5:
                return "same"
            else:
                return "diff"

        return "sku-not-found"

    def get_last_doc_index(self, batch_list, current_index, max_index, size_limit):

        current_size = 0

        while (current_index < max_index):

            this_size = sys.getsizeof(json.dumps(batch_list[current_index]))

            if ((current_size + this_size) > size_limit):
                return current_index
            else:
                current_size += this_size
                current_index += 1

        return current_index

    def create_batch_dir(self, root, name):

        if os.path.isdir(root):
            try:
                os.makedirs("/".join([root, name]))
                return True
            except OSError as e:
                if e.errno != errno.EEXIST:
                    #TODO: send rc alerts
                    raise
                else:
                    return True
        else:
            # Given root is not a valid path
            #TODO: send rc alert
            return False

    def get_all_fieldname(self):
        all_fieldname = list({k for sku in self.products.keys() for d in self.products[sku] for k in d.keys()})
        print ",".join(all_fieldname)
        
